from time import timezone
from django.db import models

class producto (models.Model):
    id=models.BigAutoField(primary_key=True)
    precio=models.IntegerField()
    def __str__(self):
        texto='{}'.format(self.precio)
        return texto

class detalle (models.Model):
    idfactura=models.BigAutoField(primary_key=True)
    producto=models.ForeignKey(producto, on_delete=models.CASCADE)
    cantidadvendida=models.IntegerField()
    def __str__(self):
        texto='{} {}'.format(self.producto,
        self.cantidadvendida)
        return texto

class cliente (models.Model):
    id=models.BigAutoField(primary_key=True)
    apellido=models.CharField(max_length=30)
    nombre=models.CharField(max_length=30)
    def __str__(self):
        texto='{} {}'.format(self.apellido,
        self.nombre)
        return texto

class factura (models.Model):
    cliente=models.ForeignKey(cliente, on_delete=models.CASCADE)
    fecha=models.DateField()
    condiciondeventa=models.CharField(max_length=30)
    def __str__(self):
        texto='{} {}'.format(self.cliente,
        self.fecha,
        self.condiciondeventa)
        return texto

# Create your models here.
